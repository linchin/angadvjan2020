import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { of } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { PostService } from 'src/app/services/post.service';
// import { UserService } from 'src/app/services/user.service';
// import { Post } from '../../things/post'

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostListComponent implements OnInit {

  errorMessage: string
  // post$ = this.postService.post$
  //   .pipe(
  //     catchError(error => {
  //       this.errorMessage = error
  //       return of(null)
  //     })
  //   )

  // user$ = this.userService.user$
  //   .pipe(
  //     catchError(error => {
  //       this.errorMessage = error
  //       return of(null)
  //     })
  //   )

    postWithUser$ = this.postService.postWithUser$
      .pipe(
        catchError( error=>{
          this.errorMessage = error
          return of(null)
        } )
      )

  constructor(
    private postService: PostService,
    // private userService: UserService
  ) { }

  ngOnInit() {
  }

}
