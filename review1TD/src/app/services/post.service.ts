import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { of, combineLatest } from 'rxjs'
import { tap, catchError, map, shareReplay } from 'rxjs/operators'

import { UserService } from 'src/app/services/user.service';
import { Post } from '../things/post'

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private postUrl: string = 'http://jsonplaceholder.typicode.com/posts'
  errorMessage: string

  post$ = this.http.get<Post[]>(this.postUrl)
    .pipe(
      tap(
        console.log
      ),
      shareReplay(99),
      catchError(error => {
        this.errorMessage = error
        return of(null)
      })
    )

  // combine BOTH service streams
  postWithUser$ = combineLatest(this.post$, this.userService.user$) //this.user$)
    .pipe(
      // tap(console.table),
      // tap(data => console.log('latest', JSON.stringify(data))),
      map(
        ([posts, users]) =>
          posts.map(
            // CAREFUL we might need a return statement if use extra {}
            p => (
              {
                ...p,
                user: users.find(
                  u => u.id === p.userId
                ).name
              } as Post
            )
          ) 
      )
    )

  constructor(
    private http: HttpClient,
    private userService: UserService) { }
}
