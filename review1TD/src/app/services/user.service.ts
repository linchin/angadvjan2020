import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { of } from 'rxjs'
import { tap, catchError, shareReplay } from 'rxjs/operators'
import { User } from '../things/user'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl:string = 'http://jsonplaceholder.typicode.com/users'
  errorMessage:string

  user$ = this.http.get<User[]>(this.userUrl)
    .pipe(
      tap(console.log),
      shareReplay(99),
      catchError( error=>{
        this.errorMessage = error
        return of(null)
      } )
    )

  constructor(private http:HttpClient) { }
}
